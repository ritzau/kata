/* @flow */

export default function findSumPairBrute(sum: number, list: [number]): boolean {
  for (var i = 0; i < list.length; ++i) {
    for (var j = i + i; j < list.length; ++j) {
      if (list[i] + list[j] == sum) {
        return true
      }
    }
  }

  return false
}

function findSumPair(sum: number, list: [number]): boolean {
  if (!list) return false
  return true
}

