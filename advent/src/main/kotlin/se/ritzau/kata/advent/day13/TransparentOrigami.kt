package se.ritzau.kata.advent.day13

import kotlin.math.max

internal fun day13a(input: String): Int {
    val origami = input.parseInput()

    return input.lines()
        .takeLastWhile { it.startsWith("fold along ") }
        .take(1)
        .map { it.substring("fold along ".length) }
        .map { it.split("=") }
        .fold(origami) { o, (axis, value) -> o.fold(axis, value.toInt()) }
        .dotCount
}

internal fun day13b(input: String): Origami {
    val origami = input.parseInput()

    return input.lines()
        .takeLastWhile { it.startsWith("fold along ") }
        .map { it.substring("fold along ".length) }
        .map { it.split("=") }
        .fold(origami) { o, (axis, value) -> o.fold(axis, value.toInt()) }
}

internal class Origami(val paper: List<List<Boolean>>) {
    val dotCount = paper.flatten().count { it }

    fun fold(axis: String, value: Int): Origami {
        fun defaultRow(): List<Boolean> = List(paper.first().size) { false }

        return when (axis) {
            "y" -> {
                Origami((max(paper.lastIndex - value, value) downTo 1)
                    .map {
                        paper.getOrElse(value - it) { defaultRow() }
                            .zip(paper.getOrElse(value + it) { defaultRow() })
                            .map { (a, b) -> a or b }
                    })
            }
            "x" -> {
                Origami(paper.map { line ->
                    (max(line.lastIndex - value, value) downTo 1)
                        .map {
                            line.getOrElse(value - it) { false } or
                                    line.getOrElse(value + it) { false }
                        }
                })
            }
            else -> throw IllegalArgumentException()
        }
    }

    override fun toString(): String {
        return "\n" + paper.joinToString("\n") {
            it.joinToString(" ") { if (it) "#" else "." }
        }
    }
}

private fun String.parseInput(): Origami {
    val (maxX, maxY) = lines()
        .asSequence()
        .takeWhile { it.isNotEmpty() }
        .map { it.split(",") }
        .map { (x, y) -> x.toInt() to y.toInt() }
        .fold(0 to 0) { (maxX, maxY), (x, y) -> max(maxX, x) to max(maxY, y) }

    val paper = List(1 + maxY) { MutableList(1 + maxX) { false } }

    lines()
        .takeWhile { it.isNotEmpty() }
        .map { it.split(",") }
        .map { (x, y) -> x.toInt() to y.toInt() }
        .forEach { (x, y) -> paper[y][x] = true }

    return Origami(paper)
}
