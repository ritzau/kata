package se.ritzau.kata.advent.day10

import java.lang.Thread.yield
import se.ritzau.kata.advent.LOG
import se.ritzau.kata.advent.log

open class ParseException(msg: String, val pos: Int) : Exception(msg)

class UnexpectedEolException(pos: Int, val expecting: String = "") : ParseException("Unexpected EOL", pos)

class IllegalScopeException(val illegalChar: Char, pos: Int) : ParseException("Bad scope '$illegalChar'", pos) {
    val score
        get() = when (illegalChar) {
            ')' -> 3
            ']' -> 57
            '}' -> 1197
            '>' -> 25137
            else -> 0
        }
}

internal fun day10a(input: String): Int {
    LOG = false

    return sequence {
        input.parseInput().forEach {
            log("Parsing: '$it'")
            try {
                Parser(it).parse()
            } catch (ex: IllegalScopeException) {
                log("${ex.illegalChar}@${ex.pos}: ${ex.score}")
                yield(ex.score)
            } catch (ex: UnexpectedEolException) {
                log("$it - Complete by adding ${ex.expecting}")
            } catch (ex: ParseException) {
                log("@${ex.pos}: $ex")
            } catch (ex: Exception) {
                println(ex)
            }
        }
    }.sum()
}

@OptIn(ExperimentalStdlibApi::class)
internal fun day10b(input: String): Long {
    LOG = false

    val scores = buildList {
        input.parseInput().forEach {
            log("Parsing: '$it'")
            try {
                Parser(it).parse()
            } catch (ex: IllegalScopeException) {
                log("${ex.illegalChar}@${ex.pos}: ${ex.score}")
            } catch (ex: UnexpectedEolException) {
                log("$it - Complete by adding ${ex.expecting}")
                add(completionScore(ex.expecting))
            } catch (ex: ParseException) {
                log("@${ex.pos}: $ex")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    return scores.sorted()[scores.size / 2]
}

private fun String.parseInput() = lines()

private class Parser(val code: String) {
    var pos = 0
    val hasMore get() = pos in code.indices
    val current get() = if (hasMore) code[pos] else throw UnexpectedEolException(pos)

    fun parse() {
        while (hasMore) {
            expr(0)
        }
    }

    fun expr(depth: Int) {
        while (hasMore) {
            when {
                expect('(', depth) -> parseAndRequire(')', depth)
                expect('{', depth) -> parseAndRequire('}', depth)
                expect('[', depth) -> parseAndRequire(']', depth)
                expect('<', depth) -> parseAndRequire('>', depth)
                else -> return
            }
        }
    }

    private fun parseAndRequire(char: Char, depth: Int) {
        try {
            expr(depth + 1)
            require(char, depth)
        } catch (ex: UnexpectedEolException) {
            throw UnexpectedEolException(ex.pos, ex.expecting + char)
        }
    }

    private fun expect(char: Char, depth: Int): Boolean {
        if (current != char) return false

        log("Current: ${(List(depth) { "|  " }).joinToString("")}$char")

        next(depth)
        return true;
    }

    private fun require(char: Char, depth: Int) {
        if (current != char) {
            throw IllegalScopeException(current, pos)
        }

        log("Current: ${(List(depth) { "|  " }).joinToString("")}$char")
        next(depth)
    }

    @Suppress("UNUSED_PARAMETER")
    private fun next(depth: Int) {
        ++pos
    }
}

private fun completionScore(string: String) =
    string.fold(0L) { total, c ->
        5 * total + when (c) {
            ')' -> 1
            ']' -> 2
            '}' -> 3
            '>' -> 4
            else -> 0
        }
    }
