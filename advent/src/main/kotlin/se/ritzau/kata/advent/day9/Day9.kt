package se.ritzau.kata.advent.day9

import kotlin.streams.toList

data class DataPoint(val value: Int, val marked: Boolean = false, var basin: Int = 0) {
    override fun toString() = if (marked) "($value:$basin)" else " $value:$basin "
}

internal fun day9a(input: String): Int {
    val heightMap = input.parseInput()
//        .also { println(it.heightMapToString())}

    val lowPoints = heightMap.findLowPoints()
    return lowPoints.score()
}

internal fun day9b(input: String): Int {
    val heightMap = input.parseInput()
//        .also { println(it.heightMapToString())}

    heightMap.findLowPoints()
        .map { it.first }
        .forEachIndexed { index, coord -> heightMap.floodFill(coord, 1 + index) }

    return heightMap.flatten()
        .map(DataPoint::basin)
        .groupingBy { it }
        .eachCount()
        .filter { it.key != 0 }
        .values
        .sortedDescending()
        .take(3)
        .reduce(Int::times)
}

private fun String.parseInput() = lines().map { line ->
    line.chars().toList().map { DataPoint(it.toChar().digitToInt()) }
}

private fun List<List<DataPoint>>.heightMapToString(): String {
    return joinToString("\n") {
        it.joinToString("")
    }
}

private fun List<List<DataPoint>>.findLowPoints(): List<Pair<Pair<Int, Int>, Pair<DataPoint, List<DataPoint>>>> {
    val coords = flatMapIndexed { y, row -> row.indices.map { x -> x to y } }
//        .also(::println)

    val neighbours = neighbours(coords)
//        .also(::println)

    val lowPoints = neighbours.map { (coord, list) -> coord to (get(coord) to list.map(this::get)) }
        .filter { (_, pair) ->
            val (value, ns) = pair
            value.value < requireNotNull(ns.minByOrNull { it.value }).value
        }
//        .also(::println)

    return lowPoints
}

private fun List<Pair<Pair<Int, Int>, Pair<DataPoint, List<DataPoint>>>>.score(): Int {
    return map { (_, pair) -> 1 + pair.first.value }.sumOf { it }
//        .also(::println)
}

private fun List<List<DataPoint>>.get(coord: Pair<Int, Int>): DataPoint {
    return get(coord.first, coord.second)
}

private fun List<List<DataPoint>>.get(x: Int, y: Int): DataPoint {
    return get(y)[x]
}

private fun List<List<DataPoint>>.neighbours(coords: List<Pair<Int, Int>>) =
    coords.map { (x, y) ->
        (x to y) to listOf(x to y - 1, x to y + 1, x - 1 to y, x + 1 to y)
            .filter { (x, y) -> isValidCoordinate(y, x) }
    }

private fun List<List<DataPoint>>.isValidCoordinate(y: Int, x: Int) = (y in indices) && (x in get(y).indices)

private fun List<List<DataPoint>>.floodFill(coord: Pair<Int, Int>, basin: Int) {
    val data = get(coord)
    if (data.value >= 9 || data.basin != 0) return

    data.basin = basin
    val (x, y) = coord
    (x to y) to listOf(x to y - 1, x to y + 1, x - 1 to y, x + 1 to y)
        .filter { (x, y) -> isValidCoordinate(y, x) }
        .filter { nbCoord -> get(nbCoord).value >= data.value }
        .forEach { floodFill(it, basin) }
}
