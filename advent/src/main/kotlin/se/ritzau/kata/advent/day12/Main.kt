package se.ritzau.kata.advent.day12

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day 12.1")
    day12a(sample1).also { println("Sample 1: $it") }
    day12a(sample2).also { println("Sample 2: $it") }
    day12a(sample3).also { println("Sample 3: $it") }
    day12a(input).also { println("Input: $it") }

    printTitle("Day 12.1")
    day12b(sample1).also { println("Sample 1: $it") }
    day12b(sample2).also { println("Sample 2: $it") }
    day12b(sample3).also { println("Sample 3: $it") }
    day12b(input).also { println("Input: $it") }
}
