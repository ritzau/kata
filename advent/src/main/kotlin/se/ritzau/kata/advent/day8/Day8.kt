package se.ritzau.kata.advent.day8

import kotlin.streams.toList
import java.util.TreeMap

internal fun day8a(input: String): Int {
    val codes = input.parseInput()

    return codes.map { it.second }
        .flatten()
        .map(String::length)
        .count { code -> code == 2 || code == 3 || code == 4 || code == 7 }
}

internal fun day8b(input: String) = input.parseInput().sumOf(::crackCode)

private fun String.parseInput(): List<Pair<List<String>, List<String>>> {
    return trimIndent().lines()
        .map { line -> line.split(" | ") }
        .map { it.map { codes -> codes.split(" ") } }
        .map { (input, output) -> input to output }
}

private fun crackCode(data: Pair<List<String>, List<String>>): Int {
    val (inputs, outputs) = data
    val digitMapping = crackTheCode(buildCodeHistogram(inputs))
    return outputs.decodeDigits(digitMapping)
}

private fun buildCodeHistogram(inputs: List<String>): TreeMap<Int, List<Set<Char>>> {
    val dict = TreeMap<Int, List<Set<Char>>>()
    inputs.forEach { segments ->
        dict.merge(segments.length, listOf(segments.toCharacterSet()), List<Set<Char>>::plus)
    }

    return dict
}

private fun String.toCharacterSet() = chars().toList().map { it.toChar() }.toSet()

private fun crackTheCode(dict: TreeMap<Int, List<Set<Char>>>): List<Set<Char>> {
    val s2 = dict.getValue(2).single()
    val s3 = dict.getValue(3).single()
    val s4 = dict.getValue(4).single()

    // 3 - 2 -> a
    val a = (s3 - s2).single()
    // 5 - a - 4 -> 1s -> g
    val g = dict.getValue(5).map { it - s4 - a }.singleOut()
    // 5 - ag - 4 -> 1s -> e
    val e = dict.getValue(5).map { it - s4 - a - g }.singleOut()
    // 5 - aeg - 2 -> 1s -> d
    val d = dict.getValue(5).map { it - s2 - a - e - g }.singleOut()
    // 5 - adeg -> 1s -> c
    val c = dict.getValue(5).map { it - a - d - e - g }.singleOut()
    // 5 - acdeg -> 1s -> f
    val f = dict.getValue(5).map { it - a - c - d - e - g }.singleOut()
    // 5 - acdefg -> 1s -> b
    val b = dict.getValue(5).map { it - a - c - d - e - f - g }.singleOut()

    return listOf(
        setOf(a, b, c, e, f, g),
        setOf(c, f),
        setOf(a, c, d, e, g),
        setOf(a, c, d, f, g),
        setOf(b, c, d, f),
        setOf(a, b, d, f, g),
        setOf(a, b, d, e, f, g),
        setOf(a, c, f),
        setOf(a, b, c, d, e, f, g),
        setOf(a, b, c, d, f, g),
    )
}

private fun List<String>.decodeDigits(digits: List<Set<Char>>) =
    map(String::toCharacterSet)
        .map(digits::indexOf)
        .digitsToInt()

private fun List<Int>.digitsToInt(): Int = fold(0) { value, digit -> 10 * value + digit }

private fun List<Set<Char>>.singleOut() =
    filter { it.size == 1 }.fold(emptySet(), Set<Char>::plus).single()
