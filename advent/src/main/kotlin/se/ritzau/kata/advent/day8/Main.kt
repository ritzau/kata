package se.ritzau.kata.advent.day8

import se.ritzau.kata.advent.printTitle

fun main() {
    // https://adventofcode.com/2021/day/8

    printTitle("Day 8.1")
    println("Sample: ${day8a(sample)}")
    println("Input: ${day8a(input)}")

    printTitle("Day 8.2")
    println("Sample: ${day8b(sample)}")
    println("Input: ${day8b(input)}")
}
