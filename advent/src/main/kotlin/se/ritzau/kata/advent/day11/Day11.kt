package se.ritzau.kata.advent.day11

data class DumboOctopus(var energy: Int) {
    var hasFlashed = false
    val willFlash get() = energy > 9 && !hasFlashed

    override fun toString(): String {
        return if (energy < 10) energy.toString() else "*"
    }
}

class Board(private val values: List<List<DumboOctopus>>) {
    override fun toString() = values.joinToString("\n") { it.joinToString("")}

    fun step(): Int {
        check(!values.flatten().any { it.hasFlashed })

        values.flatten().forEach { ++it.energy }

        while (true) {
            values.forEachIndexed { y, row ->
                row.forEachIndexed { x, element ->
                    if (element.willFlash) {
                        (-1..1).flatMap { dx -> (-1..1).map { dy -> dx to dy } }
                            .filterNot { it == 0 to 0 }
                            .map { (dx, dy) -> x + dx to y + dy }
                            .mapNotNull(this::get)
                            .filterNot { it.hasFlashed }
                            .forEach { ++it.energy }

                        element.apply {
                            hasFlashed = true
                            energy = 0
                        }
                    }
                }
            }
            if (!values.flatten().any(DumboOctopus::willFlash)) break
        }

        check(values.flatten().filter { it.hasFlashed }.all { it.energy == 0})

        return values.flatten().count(DumboOctopus::hasFlashed)
            .also { values.flatten().forEach { it.hasFlashed = false } }
    }

    private fun get(coord: Pair<Int, Int>) = get(coord.first, coord.second)
    private fun get(x: Int, y: Int) = values.getOrNull(y)?.getOrNull(x)
}

internal fun day11a(input: String): Int {
    val board = input.parseInput()

    return generateSequence { board.step() }
        .take(100)
        .sumOf { it }
}


internal fun day11b(input: String): Int {
    val board = input.parseInput()

    return generateSequence { board.step() }.withIndex()
        .first { (_, it) -> it == 100 }
        .index + 1
}

private fun String.parseInput() =
    Board(lines().map { it.map(Char::digitToInt).map(::DumboOctopus) })
