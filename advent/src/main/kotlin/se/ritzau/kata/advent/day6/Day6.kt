package se.ritzau.kata.advent.day6

fun main() {
    println("Day 6a")
    println("Sample(18): ${day6(sample, 18)}")
    println("Sample(80): ${day6(sample, 80)}")
    println("Sample(256): ${day6(sample, 256)}")
    println("Input(80): ${day6(input, 80)}")
    println("Input(256): ${day6(input, 256)}")
}

fun day6(input: String, days: Int): Long {
    val initialState = input.parseInput()
    var state = LongArray(9)
    initialState.forEach { ++state[it] }

    repeat(days) { state = step(state) }

    return state.sum()
}

private fun String.parseInput() = split(",").map { it.toInt() }

private fun step(state: LongArray): LongArray {
    val newState = LongArray(9)

    state.forEachIndexed { index, count ->
        if (index == 0) {
            newState[8] = count
            newState[6] = count
        }
        else newState[index - 1] += count
    }

    return newState
}

private const val sample = "3,4,3,1,2"

private const val input = "1,2,4,5,5,5,2,1,3,1,4,3,2,1,5,5,1,2,3,4,4,1,2,3,2,1,4,4,1,5,5,1,3,4,4,4,1,2,2,5,1,5,5,3,2,3,1,1,3,5,1,1,2,4,2,3,1,1,2,1,3,1,2,1,1,2,1,2,2,1,1,1,1,5,4,5,2,1,3,2,4,1,1,3,4,1,4,1,5,1,4,1,5,3,2,3,2,2,4,4,3,3,4,3,4,4,3,4,5,1,2,5,2,1,5,5,1,3,4,2,2,4,2,2,1,3,2,5,5,1,3,3,4,3,5,3,5,5,4,5,1,1,4,1,4,5,1,1,1,4,1,1,4,2,1,4,1,3,4,4,3,1,2,2,4,3,3,2,2,2,3,5,5,2,3,1,5,1,1,1,1,3,1,4,1,4,1,2,5,3,2,4,4,1,3,1,1,1,3,4,4,1,1,2,1,4,3,4,2,2,3,2,4,3,1,5,1,3,1,4,5,5,3,5,1,3,5,5,4,2,3,2,4,1,3,2,2,2,1,3,4,2,5,2,5,3,5,5,1,1,1,2,2,3,1,4,4,4,5,4,5,5,1,4,5,5,4,1,1,5,3,3,1,4,1,3,1,1,4,1,5,2,3,2,3,1,2,2,2,1,1,5,1,4,5,2,4,2,2,3"
