package se.ritzau.kata.advent

var LOG = false

fun printTitle(title: String) {
    println()
    println(title)
    println(String(CharArray(title.length) { '=' }))
}

fun log(msg: String) {
    if (LOG) println(msg)
}
