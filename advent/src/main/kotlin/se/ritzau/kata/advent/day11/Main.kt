package se.ritzau.kata.advent.day11

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day 11.1")
    day11a(sample).also { println("Sample: $it") }
    day11a(input).also { println("Input: $it") }

    printTitle("Day 11.2")
    day11b(sample).also { println("Sample: $it") }
    day11b(input).also { println("Input: $it") }
}
