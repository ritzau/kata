package se.ritzau.kata.advent.day12

internal val sample1 = """
    start-A
    start-b
    A-c
    A-b
    b-d
    A-end
    b-end
    """.trimIndent()

internal val sample2 = """
    dc-end
    HN-start
    start-kj
    dc-start
    dc-HN
    LN-dc
    HN-end
    kj-sa
    kj-HN
    kj-dc
    """.trimIndent()

internal val sample3 = """
    fs-end
    he-DX
    fs-he
    start-DX
    pj-DX
    end-zg
    zg-sl
    zg-pj
    pj-he
    RW-he
    fs-DX
    pj-RW
    zg-RW
    start-pj
    he-WI
    zg-he
    pj-fs
    start-RW
    """.trimIndent()

val input = """
    pq-GX
    GX-ah
    mj-PI
    ey-start
    end-PI
    YV-mj
    ah-iw
    te-GX
    te-mj
    ZM-iw
    te-PI
    ah-ZM
    ey-te
    ZM-end
    end-mj
    te-iw
    te-vc
    PI-pq
    PI-start
    pq-ey
    PI-iw
    ah-ey
    pq-iw
    pq-start
    mj-GX
    """.trimIndent()
