package se.ritzau.kata.advent.day9

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day 9.1")
    day9a(sample).also { println("Sample: $it")}
    day9a(input).also { println("Input: $it")}

    printTitle("Day 9.2")
    day9b(sample).also { println("Sample: $it")}
    day9b(input).also { println("Input: $it")}
}
