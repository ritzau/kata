package se.ritzau.kata.advent.day10

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day 10.1")
    day10a(sample).also { println("Sample: $it") }
    day10a(input).also { println("Input: $it") }

    printTitle("Day 10.2")
    day10b(sample).also { println("Sample: $it") }
    day10b(input).also { println("Input: $it") }
}
