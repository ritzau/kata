package se.ritzau.kata.advent.day12

internal fun day12a(input: String): Int {
    val start = input.parseInput()
    val paths = walk(start)

    return paths.size
}

internal fun day12b(input: String): Int {
    val start = input.parseInput()
    val paths = walk2(start)

    return paths.size
}

class Cave(val name: String) {
    val neighbours = mutableSetOf<Cave>()

    override fun hashCode(): Int {
        return name.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return (other as? Cave)?.name?.equals(name) ?: false
    }

    override fun toString(): String {
        return toString(0, mutableSetOf())
    }

    private fun toString(depth: Int, seen: MutableSet<Cave>): String {
        val head = List(depth) { "|  "}.joinToString("") + "+ $name\n"

        if (this in seen) {
            return head
        }
        seen.add(this)

        return head +
                neighbours
                    .joinToString("") { it.toString(depth + 1, seen) }
    }
}

private fun String.parseInput(): MutableMap<String, Cave> {
    val caves = mutableMapOf<String, Cave>()

    lines().map { line -> line.split("-") }
        .map { (c1, c2) ->
            val cave1 = caves.getOrPut(c1) { Cave(c1) }
            val cave2 = caves.getOrPut(c2) { Cave(c2) }
            cave1.neighbours.add(cave2)
            cave2.neighbours.add(cave1)
        }

    return caves
}

private fun walk(caves: Map<String, Cave>): MutableList<List<Cave>> {
    val start = caves.getValue("start")
    val path = listOf(start)
    val result = mutableListOf<List<Cave>>()

    walk(caves, path, result)

    return result
}

private fun walk(caves: Map<String, Cave>, path: List<Cave>, results: MutableList<List<Cave>>) {
    val current = path.last()
    if (current.name == "end") {
        results.add(path)
        return
    }

    current.neighbours.forEach { cave ->
        if (cave.name[0].isUpperCase() || cave !in path) {
            walk(caves, path + cave, results)
        }
    }
}

private fun walk2(caves: Map<String, Cave>): MutableList<List<Cave>> {
    val start = caves.getValue("start")
    val path = listOf(start)
    val result = mutableListOf<List<Cave>>()

    walk2(caves, false, path, result)

    return result
}

private fun walk2(caves: Map<String, Cave>, doneDup: Boolean, path: List<Cave>, results: MutableList<List<Cave>>) {
    val current = path.last()
    if (current.name == "end") {
        results.add(path)
        return
    }

    current.neighbours.forEach { cave ->
        val isSmall = cave.name[0].isLowerCase()
        val visitCount = path.count { it == cave }

        if (!isSmall || cave !in path) {
            walk2(caves, doneDup, path + cave, results)
        }
        else if (!doneDup && isSmall && visitCount < 2 &&  cave.name != "start") {
            walk2(caves, true, path + cave, results)
        }
    }
}
