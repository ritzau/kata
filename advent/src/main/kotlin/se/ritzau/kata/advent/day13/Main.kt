package se.ritzau.kata.advent.day13

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day 13: Transparent Origami - Part I")
    day13a(sample).also { println("Sample: $it")}
    day13a(input).also { println("Input: $it")}

    printTitle("Day 13: Transparent Origami - Part II")
    day13b(sample).also { println("Input: $it")}
    day13b(input).also { println("Input: $it")}
}
