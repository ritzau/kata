package se.ritzau.kata.advent.day14

internal fun day14a(input: String): Int {
    val p = (1..10).fold(input.parseInputForBruteforce()) { it, _ -> it.step() }

    return p.template.groupingBy { it }
        .eachCount()
        .entries
        .sortedBy { it.value }
        .let { it.last().value - it.first().value }
}

internal fun day14b(input: String): Long {
    val polymerization = input.parseInput()
    val finalState = (1..40).fold(polymerization) { it, _ -> it.step() }
    val counts = finalState.letters().map { it.value }.toLongArray()

    return counts.maxOf { it } - counts.minOf { it }
}

data class Polymerization(
    val originalTemplate: String,
    val template: Map<String, Long>,
    val rules: Map<String, Char>,
) {

    fun step(): Polymerization {
        val newTemplate = template.toMutableMap()
        val pairs = newTemplate.keys.toList()
        pairs.forEach { pair ->
            val insertion = rules[pair]
            if (insertion != null) {
                template[pair]?.let { count ->
                    newTemplate.merge(pair, -count, Long::plus)
                    newTemplate.merge("${pair[0]}$insertion", count, Long::plus)
                    newTemplate.merge("$insertion${pair[1]}", count, Long::plus)
                }
            }
        }

        newTemplate.entries.toList().forEach { entry -> if (entry.value <= 0) newTemplate.remove(entry.key) }

        return Polymerization(originalTemplate, newTemplate, rules)
    }

    fun letters(): MutableMap<Char, Long> {
        val letters = mutableMapOf<Char, Long>()

        template.entries
            .flatMap { listOf(it.key[0] to it.value, it.key[1] to it.value) }
            .forEach { (letter, value) -> letters.merge(letter, value, Long::plus) }

        letters.computeIfPresent(originalTemplate.first()) { _, prev -> prev + 1 }
        letters.computeIfPresent(originalTemplate.last()) { _, prev -> prev + 1 }

        letters.entries.forEach { entry -> entry.setValue(entry.value / 2) }

        return letters
    }
}

data class BruteForcePolymerization(val template: String, val rules: Map<String, Char>) {
    fun step(): BruteForcePolymerization {
        val sb = StringBuilder(2 * template.length)
        var buf = ""

        template.forEach { c ->
            when {
                buf.isEmpty() -> {
                    buf = "$c"
                    sb.append(c)
                }
                buf.length == 1 -> {
                    buf = "$buf$c"
                    rules[buf]?.let { sb.append(it) }
                    sb.append(c)
                }
                else -> {
                    buf = "${buf[1]}$c"
                    rules[buf]?.let { sb.append(it) }
                    sb.append(c)
                }
            }
        }

        return BruteForcePolymerization(sb.toString(), rules)
    }
}

private fun String.parseInputForBruteforce(): BruteForcePolymerization {
    val template = lines().take(1).single()
    val rules = lines().takeLastWhile { it.isNotEmpty() }
        .map { it.split(" -> ") }
        .associateBy({ (k, _) -> k }, { (_, v) -> v[0] })
        .toSortedMap()

    return BruteForcePolymerization(template, rules)
}

private fun String.parseInput(): Polymerization {
    val template = lines().take(1).single()
    val rules = lines().takeLastWhile { it.isNotEmpty() }
        .map { it.split(" -> ") }
        .associateBy({ (k, _) -> k }, { (_, v) -> v[0] })
        .toSortedMap()

    val pairs = mutableMapOf<String, Long>()
    template.windowed(2).forEach { pair ->
        pairs.compute(pair) { _, v -> (v ?: 0) + 1 }
    }

    return Polymerization(template, pairs.toMap(), rules)
}
