package se.ritzau.kata.advent.day14

import se.ritzau.kata.advent.printTitle

fun main() {
    printTitle("Day14: Extended Polymerization - Part I")
    day14a(sample).also { println("Sample: $it") }
    day14a(input).also { println("Input: $it") }

    printTitle("Day14: Extended Polymerization - Part II")
    day14b(sample).also { println("Sample: $it") }
    day14b(input).also { println("Input: $it") }
}
