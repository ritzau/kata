/* @flow */

export default function addBinary(a : string, b: string): string {
  var zero = '0'.charCodeAt(0);

  function digit_at(string, index) {
      return index >= 0 ? (string.charCodeAt(index) - zero) : 0
  }

  var result_length = Math.max(a.length, b.length) + 1

  var result = [],
      carry = 0;

  for (var index = 1; index <= result_length; ++index) {
    var digit_a = digit_at(a, a.length - index),
        digit_b = digit_at(b, b.length - index);

    result[result_length - index] = carry ^ digit_a ^ digit_b;
    carry = (carry & digit_a) | (carry & digit_b) | (digit_a & digit_b);
  }

  return result.join('');
}
