def addBinary(a, b):
    def digit_at(string, index):
        return ord(string[index]) - ord('0')

    result_length = 1 + max(len(a), len(b))
    result = result_length * [None]
    carry = 0
    
    for i in range(1, result_length + 1):
        digit_a = digit_at(a, -i) if i <= len(a) else 0
        digit_b = digit_at(b, -i) if i <= len(b) else 0

        result[-i] = carry ^ digit_a ^ digit_b
        carry = (carry & digit_a) | (carry & digit_b) | (digit_a & digit_b)

    return ''.join(map(str, result))

print(addBinary("", ""))
print(addBinary("0", "0"))
print(addBinary("11", "11"))
print(addBinary("10", "10"))
