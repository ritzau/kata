jest.dontMock('../addBinary');

describe('addBinary', function() {
  it('adds "" + "" to equal "0"', function() {
     var addBinary = require('../addBinary');
     expect(addBinary("", "")).toBe("0");
  });

  it('adds "" + "0" to equal "00"', function() {
     var addBinary = require('../addBinary');
     expect(addBinary("", "0")).toBe("00");
  });

  it('adds "0" + "" to equal "00"', function() {
     var addBinary = require('../addBinary');
     expect(addBinary("0", "")).toBe("00");
  });

  it('adds "" + "1010" to equal "01010"', function() {
     var addBinary = require('../addBinary');
     expect(addBinary("", "1010")).toBe("01010");
  });

  it('adds "1111" + "" to equal "01111"', function() {
     var addBinary = require('../addBinary');
     expect(addBinary("1111", "")).toBe("01111");
  });

  it('adds "0" + "0" to equal "00"', function() {
    var addBinary = require('../addBinary');
    expect(addBinary("0", "0")).toBe("00");
  });

  it('adds "1" + "0" to equal "01"', function() {
    var addBinary = require('../addBinary');
    expect(addBinary("1", "0")).toBe("01");
  });

  it('adds "1" + "1" to equal "10"', function() {
    var addBinary = require('../addBinary');
    expect(addBinary("1", "1")).toBe("10");
  });
});
