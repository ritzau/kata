def iterativeBinarySearch(element, alist):
    try:
        return alist.index(element)
    except:
        return -1

def assert_equal(expected, actual):
    assert expected == actual, "assertion failed: expected={0} actual={1}".format(expected, actual)

def test_chop():
  assert_equal(-1, iterativeBinarySearch(3, []))
  assert_equal(-1, iterativeBinarySearch(3, [1]))
  assert_equal(0,  iterativeBinarySearch(1, [1]))
  
  assert_equal(0,  iterativeBinarySearch(1, [1, 3, 5]))
  assert_equal(1,  iterativeBinarySearch(3, [1, 3, 5]))
  assert_equal(2,  iterativeBinarySearch(5, [1, 3, 5]))
  assert_equal(-1, iterativeBinarySearch(0, [1, 3, 5]))
  assert_equal(-1, iterativeBinarySearch(2, [1, 3, 5]))
  assert_equal(-1, iterativeBinarySearch(4, [1, 3, 5]))
  assert_equal(-1, iterativeBinarySearch(6, [1, 3, 5]))
  
  assert_equal(0,  iterativeBinarySearch(1, [1, 3, 5, 7]))
  assert_equal(1,  iterativeBinarySearch(3, [1, 3, 5, 7]))
  assert_equal(2,  iterativeBinarySearch(5, [1, 3, 5, 7]))
  assert_equal(3,  iterativeBinarySearch(7, [1, 3, 5, 7]))
  assert_equal(-1, iterativeBinarySearch(0, [1, 3, 5, 7]))
  assert_equal(-1, iterativeBinarySearch(2, [1, 3, 5, 7]))
  assert_equal(-1, iterativeBinarySearch(4, [1, 3, 5, 7]))
  assert_equal(-1, iterativeBinarySearch(6, [1, 3, 5, 7]))
  assert_equal(-1, iterativeBinarySearch(8, [1, 3, 5, 7]))

if __name__ == "__main__":
    test_chop()
