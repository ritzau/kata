jest.dontMock('../binarySearch');

function testBinarySearch(binarySearch) {
     expect(binarySearch(3, [])).toBe(-1)
     expect(binarySearch(3, [1])).toBe(-1)
     expect(binarySearch(1, [1])).toBe(0)

     expect(binarySearch(1, [1, 3, 5])).toBe(0)
     expect(binarySearch(3, [1, 3, 5])).toBe(1)
     expect(binarySearch(5, [1, 3, 5])).toBe(2)
     expect(binarySearch(0, [1, 3, 5])).toBe(-1)
     expect(binarySearch(2, [1, 3, 5])).toBe(-1)
     expect(binarySearch(4, [1, 3, 5])).toBe(-1)
     expect(binarySearch(6, [1, 3, 5])).toBe(-1)

     expect(binarySearch(1, [1, 3, 5, 7])).toBe(0)
     expect(binarySearch(3, [1, 3, 5, 7])).toBe(1)
     expect(binarySearch(5, [1, 3, 5, 7])).toBe(2)
     expect(binarySearch(7, [1, 3, 5, 7])).toBe(3)
     expect(binarySearch(0, [1, 3, 5, 7])).toBe(-1)
     expect(binarySearch(2, [1, 3, 5, 7])).toBe(-1)
     expect(binarySearch(4, [1, 3, 5, 7])).toBe(-1)
     expect(binarySearch(6, [1, 3, 5, 7])).toBe(-1)
     expect(binarySearch(8, [1, 3, 5, 7])).toBe(-1)
}

describe('binarySearch', function() {
  it('test iterative solution', function() {
     testBinarySearch(require('../binarySearch').iterativeBinarySearch);
  });
  it('test recursive solution', function() {
     testBinarySearch(require('../binarySearch').recursiveBinarySearch);
  });
  it('test functional solution', function() {
     testBinarySearch(require('../binarySearch').functionalBinarySearch);
  });
  it('test oop solution', function() {
     testBinarySearch(require('../binarySearch').oopBinarySearch);
  });
});
