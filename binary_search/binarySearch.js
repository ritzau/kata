/* @flow */

export function iterativeBinarySearch(element: number, list: [number]): number {
  if (list.length == 0) {
    return -1;
  }

  var lessEqual = 0,
      greater = list.length;
  while (greater - lessEqual > 1) {
    var mid = lessEqual + Math.trunc((greater - lessEqual) / 2);
    if (list[mid] <= element) {
      lessEqual = mid;
    } else {
      greater = mid;
    }
  }
  return list[lessEqual] == element? lessEqual : -1;
}

export function recursiveBinarySearch(element: number, list: [number]): number {
  function search(elm, list, lessEqual, greater) {
      if (greater - lessEqual > 1) {
        var mid = lessEqual + Math.trunc((greater - lessEqual) / 2);
        if (list[mid] <= elm) {
          lessEqual = mid;
        } else {
          greater = mid;
        }
        return search(elm, list, lessEqual, greater);
      } else {
        return list[lessEqual] == elm ? lessEqual : -1;
      }
  }

  return list.length == 0 ? -1 : search(element, list, 0, list.length);
}

export function functionalBinarySearch(element: number, list: [number]): number {
  function mid(a, b) {
    return a + Math.trunc((b - a) / 2);
  }

  function start(elm, list, lessEqual, mid, greater) {
    return list[mid] <= elm ? mid : lessEqual;
  }

  function end(elm, list, lessEqual, mid, greater) {
    return list[mid] <= elm ? greater : mid;
  }

  function search(elm, list, lessEqual, greater) {
      if (greater - lessEqual > 1) {
        return search(
            elm,
            list,
            start(elm, list, lessEqual, mid(lessEqual, greater), greater),
            end(elm, list, lessEqual, mid(lessEqual, greater), greater));
      } else {
        return list[lessEqual] == elm ? lessEqual : -1;
      }
  }

  return list.length == 0 ? -1 : search(element, list, 0, list.length);
}

export function oopBinarySearch(element: number, list: [number]): number {
    class Range {
      list: [number];
      lessEqual: number;
      greater: number;

      constructor(list, start, end) {
        this.list = list;
        this.lessEqual = start;
        this.greater = end;
      }

      mid() {
        return this.lessEqual + Math.trunc((this.greater - this.lessEqual) / 2);
      }

      compare(a, b) {
          return a <= b;
      }

      search(element) {
        if (this.greater - this.lessEqual == 1) {
          return list[this.lessEqual] == element ? this.lessEqual : -1;
        }

        if (this.compare(list[this.mid()], element)) {
          return new Range(this.list, this.mid(), this.greater).search(element);
        } else {
          return new Range(this.list, this.lessEqual, this.mid()).search(element);
        }
      }
    }

    return list.length > 0 ? new Range(list, 0, list.length).search(element) : -1;
}
