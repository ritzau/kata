/* @flow */

class Node {
  value: number;
  left: ?Node;
  right: ?Node;

  constructor(value, left = null, right = null) {
    this.value = value
    this.left = left
    this.right = right
  }
}

export default function treeByLevel(root) {
  if (!root) return []

  var result = []
  var fifo = [root]
  while (fifo.length > 0) {
    var n  = fifo.length
    var levelValues = []
    for (var i = 0; i < n; ++i) {
      var node = fifo.shift()
      levelValues.push(node.value)
      if (node.left) fifo.push(node.left)
      if (node.right) fifo.push(node.right)
    }
    result.push(levelValues)
  }

  return result
}
